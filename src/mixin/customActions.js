
export const customActions = {
    methods: {
         actionsPayload() {
            return {
                saveData: {method: 'POST', url: 'alternative.json'},
                getData: {method: 'GET'}
            }
        }
    },
    created() {
        console.log('mixin created')
    }
};