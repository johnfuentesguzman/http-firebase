import Vue from 'vue'
import VueResource from 'vue-resource'; // ugin for Vue.js provides services for making web requests and handle responses
import App from './App.vue'

Vue.use(VueResource); // to add new plugin or external enhance  to VUE Instance

Vue.http.options.root = 'https://vue-resource-http-be637.firebaseio.com/'; // http is a Vue resource mettod
Vue.http.interceptors.push((request, next) => { // like the angular interceptor: any request would use this proxy first
  console.log(request);
  if (request.method == 'POST') {
    request.method = 'PUT';
  }
  next(response => { // emit a callback func wich will be catch for fetchData func (app.vue)
    response.json = () => { return {data: response.body} }
  });
});

new Vue({
  el: '#app',
  render: h => h(App)
})
